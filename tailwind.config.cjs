/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}'],
  theme: {
    extend: {
      colors: {
        'logo-blue': '#58C0F4'
      }
    },
  },
  plugins: [require("daisyui")],
  daisyui: {
    styled: false,
  },
}
